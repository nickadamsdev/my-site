import React from 'react';
import Link from 'gatsby-link';
import bgvid from '../assets/bg-vid.mp4';
import 'typeface-exo';
import 'typeface-orbitron';
import 'typeface-roboto';
import styles from './index.module.css';
console.log(styles);

export default () => (
  <div>
    <video loop autoPlay className={styles.bgvid}>
      <source src={bgvid} type="video/mp4" />
    </video>
    <div className={styles.marquee}>
      <h1>NICK ADAMS</h1>
      <h2>Frontend Developer</h2>
      <div className={styles.buttonContainer}>
        <div className={styles.buttonRow}>
          <div>
            <div className={styles.buttonForeground}>
              <h3>ME</h3>
            </div>
            <div className={styles.buttonBackground}>
              <h3>ME</h3>
            </div>
          </div>
          <div>
            <div className={styles.buttonForeground}>
              <h3>SKILLS</h3>
            </div>
            <div className={styles.buttonBackground}>
              <h3>SKILLS</h3>
            </div>
          </div>
          <div>
            <div className={styles.buttonForeground}>
              <h3>WORK</h3>
            </div>
            <div className={styles.buttonBackground}>
              <h3>WORK</h3>
            </div>
          </div>
          <div>
            <div className={styles.buttonForeground}>
              <h3>CONTACT</h3>
            </div>
            <div className={styles.buttonBackground}>
              <h3>CONTACT</h3>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
);
