import React from 'react';

export default props => (
  <div
    className={this.props.buttonStyle}
    onMouseEnter={this.props.onEnter()}
    onMouseLeave={this.props.onLeave()}
  >
    <h3>{this.props.label}</h3>
  </div>
);
