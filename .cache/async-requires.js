// prefer default export if available
const preferDefault = m => m && m.default || m

exports.components = {
  "component---cache-dev-404-page-js": require("gatsby-module-loader?name=component---cache-dev-404-page-js!/home/nicktwo/my-site/.cache/dev-404-page.js"),
  "component---src-pages-index-js": require("gatsby-module-loader?name=component---src-pages-index-js!/home/nicktwo/my-site/src/pages/index.js")
}

exports.json = {
  "dev-404-page.json": require("gatsby-module-loader?name=path---dev-404-page!/home/nicktwo/my-site/.cache/json/dev-404-page.json"),
  "index.json": require("gatsby-module-loader?name=path---index!/home/nicktwo/my-site/.cache/json/index.json")
}

exports.layouts = {

}